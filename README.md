# Conference management API
This application allows:
- Create, update and cancel conferences
- Get all conferences or a particular by id
- Check availability of the conference based on available seats in a room
- Register for and unregister from the conference
- Get conference participants  

It is also possible to create and update a user, and to get all the users or a particular one by id.

To build and run the project, use the following command:  
`mvn spring-boot:run`  
This will started the embedded server on port 8080. To specify different port, for example 8085, use this command:   
`mvn spring-boot:run -Dspring-boot.run.arguments=--server.port=8085`  
Make sure that `JAVA_HOME` points to jdk11.

To access the application, visit `http://localhost:<server.port>`.  
To access Swagger UI, visit `http://localhost:<server.port>/swagger-ui/index.html#/`
