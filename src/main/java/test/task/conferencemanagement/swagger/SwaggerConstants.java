package test.task.conferencemanagement.swagger;

public final class SwaggerConstants {
    private SwaggerConstants() {
        throw new UnsupportedOperationException();
    }

    public static final String RESOURCE_WAS_NOT_FOUND_DESCRIPTION = "Resource was not found";
    public static final String STATUS_OK = "200";
    public static final String STATUS_CREATED = "201";
    public static final String STATUS_NOT_FOUND = "404";
    public static final String STATUS_CONFLICT = "409";

    public static final String USER_CONTROLLER_TAG = "User Controller";
    public static final String USER_ALREADY_EXISTS_DESCRIPTION = "User already exists";
    public static final String USER_WAS_NOT_FOUND_DESCRIPTION = "User was not found";
    public static final String ERROR_MESSAGE_USER_NOT_FOUND_EXAMPLE = "{\"errorMessage\":\"User with id 1 was not found\"}";
    public static final String ERROR_MESSAGE_USER_ALREADY_EXISTS_EXAMPLE = "{\"errorMessage\":\"User user1 already exists\"}";
    public static final String CREATE_USER_REQUEST_BODY_EXAMPLE = "{\"username\":\"user1\",\"fullName\":\"John Doe\"}";
    public static final String USER_RESPONSE_BODY_EXAMPLE = "{\"id\":1,\"username\":\"user1\",\"fullName\":\"John Doe\"}";
    public static final String MULTIPLE_USER_RESPONSE_BODY_EXAMPLE = "[{\"id\":1,\"username\":\"user1\",\"fullName\":\"John Doe\"}, {\"id\":2,\"username\":\"user2\",\"fullName\":\"Jane Doe\"}]";
    public static final String UPDATE_USER_REQUEST_RESPONSE_BODY_EXAMPLE = "{\"id\":1,\"username\":\"user1\",\"fullName\":\"Jane Doe\"}";

    public static final String CREATE_USER_SUMMARY = "Create user";
    public static final String CREATE_USER_CREATED_DESCRIPTION = "User was created";
    public static final String UPDATE_USER_SUMMARY = "Update user";
    public static final String UPDATE_USER_OK_DESCRIPTION = "User was updated";
    public static final String GET_USERS_SUMMARY = "Get all users";
    public static final String GET_USER_BY_ID_SUMMARY = "Get user by id";
    public static final String USER_ID_PARAM_DESCRIPTION = "User Id";

    public static final String CONFERENCE_CONTROLLER_TAG = "Conference Controller";
    public static final String CONFERENCE_WAS_NOT_FOUND_DESCRIPTION = "Conference was not found";
    public static final String CONFERENCE_ID_PARAM_DESCRIPTION = "Conference Id";
    public static final String CONFERENCE_REGISTRATION_FAILURE_DESCRIPTION = "Conference registration failure";
    public static final String ERROR_MESSAGE_CONFERENCE_NOT_FOUND_EXAMPLE = "{\"errorMessage\":\"Conference with id 0 was not found\"}";
    public static final String CONFERENCE_AVAILABILITY_RESULT_DESCRIPTION = "Availability result";
    public static final String CONFERENCE_WAS_CANCELLED_DESCRIPTION = "Conference was cancelled";
    public static final String CONFERENCE_WAS_CREATED_DESCRIPTION = "Conference was created";
    public static final String CONFERENCE_WAS_UPDATED_DESCRIPTION = "Conference was updated";
    public static final String CONFERENCE_AVAILABLE_NAME = "Conference is available for registration";
    public static final String CONFERENCE_AVAILABLE_EXAMPLE = "{\"isAvailable\":true}";
    public static final String CONFERENCE_UNAVAILABLE_NAME = "Conference is unavailable for registration";
    public static final String CONFERENCE_UNAVAILABLE_EXAMPLE = "{\"isAvailable\":false}";
    public static final String CONFERENCE_REGISTRATION_FAILURE_CANCELLED_NAME = "Conference was cancelled";
    public static final String CONFERENCE_REGISTRATION_FAILURE_NO_SEATS_NAME = "There are no seats available in the conference room";
    public static final String CONFERENCE_REGISTRATION_FAILURE_CANCELLED_EXAMPLE = "{\"errorMessage\":\"The conference has been cancelled\"}";
    public static final String CONFERENCE_REGISTRATION_FAILURE_NO_SEATS_EXAMPLE = "{\"errorMessage\":\"There are no available seats\"}";
    public static final String UNREGISTER_FROM_CONFERENCE_USER_WAS_MEMBER_EXAMPLE = "{\"message\":\"User has been successfully unregistered\"}";
    public static final String UNREGISTER_FROM_CONFERENCE_USER_WAS_NOT_MEMBER_EXAMPLE = "{\"message\":\"User is already not participant of the conference\"}";
    public static final String CREATE_CONFERENCE_REQUEST_BODY_EXAMPLE = "{\"name\":\"SuccessSociety\",\"maxAvailableSeats\":931,\"cancelled\":false}";
    public static final String UPDATE_CONFERENCE_REQUEST_RESPONSE_BODY = "{\"id\":3,\"name\":\"SuccessSociety\",\"maxAvailableSeats\":931,\"cancelled\":false}";
    public static final String MULTIPLE_CONFERENCE_RESPONSE_BODY_WITHOUT_USERS_EXAMPLE =
        "[{\"id\":3,\"name\":\"SuccessSociety\",\"maxAvailableSeats\":931,\"cancelled\":false},{\"id\":4,\"name\":\"DeliciousDiscussion\",\"maxAvailableSeats\":626,\"cancelled\":false}]";
    public static final String CONFERENCE_RESPONSE_BODY_WITHOUT_USERS_EXAMPLE = "{\"id\":3,\"name\":\"SuccessSociety\",\"maxAvailableSeats\":931,\"cancelled\":false}";
    public static final String ERROR_MESSAGE_USER_NOT_FOUND_DURING_CONFERENCE_REGISTRATION_EXAMPLE =
        "{\"errorMessage\":\"User with id 0 could not register for conference, because it doesn't exists\"}";
    public static final String CONFERENCE_RESPONSE_BODY_WITH_USERS_EXAMPLE =
        "{\"id\":3,\"name\":\"SuccessSociety\",\"maxAvailableSeats\":931,\"cancelled\":false, \"participants\": [" + MULTIPLE_USER_RESPONSE_BODY_EXAMPLE + "]}";

    public static final String GET_CONFERENCES_SUMMARY = "Get all conferences";
    public static final String GET_CONFERENCE_BY_ID_SUMMARY = "Get conference by id";
    public static final String CREATE_CONFERENCE_SUMMARY = "Create conference";
    public static final String UPDATE_CONFERENCE_SUMMARY = "Update conference";
    public static final String CANCEL_CONFERENCE_SUMMARY = "Cancel conference";
    public static final String IS_AVAILABLE_FOR_REGISTRATION_SUMMARY = "Is conference available for registration";
    public static final String IS_AVAILABLE_FOR_REGISTRATION_DESCRIPTION = "The conference is available for registration if there are enough seats in the room";
    public static final String REGISTER_FOR_CONFERENCE_SUMMARY = "Register user for conference";
    public static final String UNREGISTER_FOR_CONFERENCE_SUMMARY = "Unregister user from conference";
    public static final String GET_CONFERENCE_PARTICIPANTS_SUMMARY = "Get conference participants";

}
