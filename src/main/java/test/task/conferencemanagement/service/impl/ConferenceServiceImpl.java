package test.task.conferencemanagement.service.impl;

import test.task.conferencemanagement.dto.ConferenceDto;
import test.task.conferencemanagement.dto.UserDto;
import test.task.conferencemanagement.exception.ConferenceIsUnavailableException;
import test.task.conferencemanagement.exception.ConferenceNotFoundException;
import test.task.conferencemanagement.exception.UserNotFoundException;
import test.task.conferencemanagement.model.Conference;
import test.task.conferencemanagement.model.User;
import test.task.conferencemanagement.repository.ConferenceRepository;
import test.task.conferencemanagement.repository.UserRepository;
import test.task.conferencemanagement.service.ConferenceService;
import test.task.conferencemanagement.service.Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Service
public class ConferenceServiceImpl implements ConferenceService {

    private final ConferenceRepository conferenceRepository;
    private final UserRepository userRepository;
    private final Mapper<Conference, ConferenceDto> conferenceMapper;
    private final Mapper<User, UserDto> userMapper;
    private final ConcurrentHashMap<Long, ReentrantLock> lockById = new ConcurrentHashMap<>();

    public ConferenceServiceImpl(ConferenceRepository conferenceRepository, UserRepository userRepository, Mapper<Conference, ConferenceDto> conferenceMapper, Mapper<User, UserDto> userMapper) {
        this.conferenceRepository = conferenceRepository;
        this.userRepository = userRepository;
        this.conferenceMapper = conferenceMapper;
        this.userMapper = userMapper;
    }

    @Override
    @Transactional
    public ConferenceDto saveConference(ConferenceDto conferenceDto) {
        Conference conference = conferenceMapper.toModel(conferenceDto);
        conference = conferenceRepository.save(conference);
        return conferenceMapper.toDto(conference);
    }

    @Override
    @Transactional
    public void cancelConference(Long conferenceId) {
        Conference conference = conferenceRepository.findById(conferenceId).orElseThrow(() -> createConferenceNotFoundException(conferenceId));
        conference.setCancelled(true);
        conferenceRepository.save(conference);
    }

    private ConferenceNotFoundException createConferenceNotFoundException(Long id) {
        return new ConferenceNotFoundException(String.format("Conference with id %d was not found", id));
    }

    @Override
    public List<ConferenceDto> getAll() {
        return this.conferenceRepository.findAll().stream().map(conferenceMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public boolean isAvailableForRegistration(Long id) {
        boolean conferenceAbsent = conferenceRepository.countById(id) == 0;
        if (conferenceAbsent) {
            throw new ConferenceNotFoundException(String.format("Conference with id %d was not found", id));
        }
        return conferenceRepository.isAvailableForRegistration(id) < 0;
    }

    @Override
    @Transactional
    public boolean unregister(Long conferenceId, Long userId) {
        Conference conference = conferenceRepository.findById(conferenceId).orElseThrow(() -> createConferenceNotFoundException(conferenceId));
        User userById = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(String.format("User with id %d was not found", userId)));
        boolean isRemovalSuccessful = conference.getParticipants().remove(userById);
        if (isRemovalSuccessful) {
            conferenceRepository.save(conference);
        }
        return isRemovalSuccessful;
    }

    @Override
    @Transactional
    public void register(Long conferenceId, Long userId) {
        Conference conference = conferenceRepository.findById(conferenceId).orElseThrow(() -> createConferenceNotFoundException(conferenceId));
        ReentrantLock lock = getLockByConferenceId(conferenceId);
        try {
            lock.tryLock(5, TimeUnit.SECONDS);
            if (conference.isCancelled() || !isAvailableForRegistration(conferenceId)) {
                String exceptionMessage = conference.isCancelled() ? "The conference has been cancelled" : "There are no available seats";
                throw new ConferenceIsUnavailableException(exceptionMessage);
            }
            User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(String.format("User with id %d could not register for conference, because it doesn't exists", userId)));
            conference.getParticipants().add(user);
            conferenceRepository.save(conference);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }

    }

    @Override
    public ConferenceDto getById(Long conferenceId) {
        return conferenceRepository.findById(conferenceId).map(conferenceMapper::toDto).orElseThrow(() -> createConferenceNotFoundException(conferenceId));
    }

    @Override
    public ConferenceDto getParticipants(Long conferenceId) {
        return conferenceRepository.getParticipantsByConferenceId(conferenceId).map(conference -> {
            ConferenceDto conferenceDto = conferenceMapper.toDto(conference);
            Set<UserDto> conferenceParticipants = conference.getParticipants().stream().map(userMapper::toDto).collect(Collectors.toSet());
            conferenceDto.setParticipants(conferenceParticipants);
            return conferenceDto;
        }).orElseThrow(() -> new ConferenceNotFoundException(String.format("Conference with id %d was not found", conferenceId)));
    }

    private ReentrantLock getLockByConferenceId(Long conferenceId) {
        ReentrantLock reentrantLock = new ReentrantLock();
        ReentrantLock oldLock = lockById.putIfAbsent(conferenceId, reentrantLock);
        return oldLock == null ? reentrantLock : oldLock;
    }
}
