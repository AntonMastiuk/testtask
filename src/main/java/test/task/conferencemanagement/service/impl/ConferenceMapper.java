package test.task.conferencemanagement.service.impl;

import test.task.conferencemanagement.dto.ConferenceDto;
import test.task.conferencemanagement.model.Conference;
import test.task.conferencemanagement.service.Mapper;
import org.springframework.stereotype.Service;

@Service
public class ConferenceMapper implements Mapper<Conference, ConferenceDto> {

    @Override
    public ConferenceDto toDto(Conference model) {
        ConferenceDto conferenceDto = new ConferenceDto();
        conferenceDto.setId(model.getId());
        conferenceDto.setName(model.getName());
        conferenceDto.setCancelled(model.isCancelled());
        conferenceDto.setMaxAvailableSeats(model.getMaxAvailableSeats());
        return conferenceDto;
    }

    @Override
    public Conference toModel(ConferenceDto dto) {
        Conference conference = new Conference();
        conference.setId(dto.getId());
        conference.setName(dto.getName());
        conference.setCancelled(dto.isCancelled());
        conference.setMaxAvailableSeats(dto.getMaxAvailableSeats());
        return conference;
    }
}
