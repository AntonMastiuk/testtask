package test.task.conferencemanagement.service.impl;

import test.task.conferencemanagement.dto.UserDto;
import test.task.conferencemanagement.model.User;
import test.task.conferencemanagement.service.Mapper;
import org.springframework.stereotype.Service;

@Service
public class UserMapper implements Mapper<User, UserDto> {

    @Override
    public UserDto toDto(User model) {
        UserDto dto = new UserDto();
        dto.setId(model.getId());
        dto.setFullName(model.getFullName());
        dto.setUsername(model.getUsername());
        return dto;
    }

    @Override
    public User toModel(UserDto dto) {
        User user = new User();
        user.setId(dto.getId());
        user.setFullName(dto.getFullName());
        user.setUsername(dto.getUsername());
        return user;
    }
}
