package test.task.conferencemanagement.service.impl;

import test.task.conferencemanagement.dto.UserDto;
import test.task.conferencemanagement.exception.UserAlreadyExists;
import test.task.conferencemanagement.exception.UserNotFoundException;
import test.task.conferencemanagement.model.User;
import test.task.conferencemanagement.repository.UserRepository;
import test.task.conferencemanagement.service.Mapper;
import test.task.conferencemanagement.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final Mapper<User, UserDto> userMapper;

    public UserServiceImpl(UserRepository userRepository, Mapper<User, UserDto> userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    @Transactional
    public UserDto saveUser(UserDto userDto) {
        User user = userMapper.toModel(userDto);
        int userCountByUsername = userRepository.countByUsername(user.getUsername());
        if (userCountByUsername > 0) {
            throw new UserAlreadyExists(String.format("User %s already exists", user.getUsername()));
        }
        user = userRepository.save(user);
        return userMapper.toDto(user);
    }

    @Override
    public List<UserDto> getAll() {
        return userRepository.findAll().stream().map(userMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public UserDto findUserById(Long userId) {
        return userRepository.findById(userId).map(userMapper::toDto).orElseThrow(() -> new UserNotFoundException(String.format("User with id %d was not found", userId)));
    }
}
