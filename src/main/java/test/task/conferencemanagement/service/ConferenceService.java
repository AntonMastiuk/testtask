package test.task.conferencemanagement.service;

import test.task.conferencemanagement.dto.ConferenceDto;

import java.util.List;

public interface ConferenceService {

    ConferenceDto saveConference(ConferenceDto conferenceDto);

    void cancelConference(Long id);

    List<ConferenceDto> getAll();

    boolean isAvailableForRegistration(Long id);

    boolean unregister(Long conferenceId, Long userId);

    void register(Long conferenceId, Long userId);

    ConferenceDto getById(Long conferenceId);

    ConferenceDto getParticipants(Long conferenceId);
}
