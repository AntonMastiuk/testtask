package test.task.conferencemanagement.service;

public interface Mapper<M, D> {
    D toDto(M model);

    M toModel(D dto);
}
