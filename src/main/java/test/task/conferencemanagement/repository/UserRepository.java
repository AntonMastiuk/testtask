package test.task.conferencemanagement.repository;

import test.task.conferencemanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    int countByUsername(String username);
}
