package test.task.conferencemanagement.repository;

import test.task.conferencemanagement.model.Conference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import java.util.Optional;

public interface ConferenceRepository extends JpaRepository<Conference, Long> {
    @Query(value = "select (select count (*) from conference_users cu where cu.conference_id = :conference_id) - (select c.maxavailableseats from conference c where c.id = :conference_id)",
        nativeQuery = true)
    int isAvailableForRegistration(@Param("conference_id") Long conferenceId);

    int countById(Long id);

    @Lock(LockModeType.PESSIMISTIC_READ)
    Optional<Conference> findById(Long id);

    @Query("select c from Conference c left join fetch c.participants where c.id = :conferenceId")
    Optional<Conference> getParticipantsByConferenceId(@Param("conferenceId") Long conferenceId);
}
