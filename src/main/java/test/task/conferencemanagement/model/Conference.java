package test.task.conferencemanagement.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Conference {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "conference_seq")
    @SequenceGenerator(name = "conference_seq", sequenceName = "conference_seq", allocationSize = 1)
    private Long id;
    private String name;
    @Column(name = "maxavailableseats")
    private int maxAvailableSeats;
    private boolean cancelled;
    @ManyToMany
    @JoinTable(name = "conference_users", joinColumns = @JoinColumn(name = "conference_id"), inverseJoinColumns = @JoinColumn(name = "users_id"))
    private Set<User> participants = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxAvailableSeats() {
        return maxAvailableSeats;
    }

    public void setMaxAvailableSeats(int maxAvailableSeats) {
        this.maxAvailableSeats = maxAvailableSeats;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Set<User> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<User> participants) {
        this.participants = participants;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Conference that = (Conference) o;
        return maxAvailableSeats == that.maxAvailableSeats &&
            cancelled == that.cancelled &&
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, maxAvailableSeats, cancelled);
    }
}
