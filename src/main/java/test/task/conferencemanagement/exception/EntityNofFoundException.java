package test.task.conferencemanagement.exception;

public class EntityNofFoundException extends RuntimeException {

    public EntityNofFoundException() {
        super();
    }

    public EntityNofFoundException(String message) {
        super(message);
    }

    public EntityNofFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityNofFoundException(Throwable cause) {
        super(cause);
    }
}
