package test.task.conferencemanagement.exception;

public class ConferenceNotFoundException extends EntityNofFoundException {
    public ConferenceNotFoundException() {
        super();
    }

    public ConferenceNotFoundException(String message) {
        super(message);
    }

    public ConferenceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConferenceNotFoundException(Throwable cause) {
        super(cause);
    }
}
