package test.task.conferencemanagement.exception;

public class ConferenceIsUnavailableException extends RuntimeException {

    public ConferenceIsUnavailableException() {
        super();
    }

    public ConferenceIsUnavailableException(String message) {
        super(message);
    }

    public ConferenceIsUnavailableException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConferenceIsUnavailableException(Throwable cause) {
        super(cause);
    }
}
