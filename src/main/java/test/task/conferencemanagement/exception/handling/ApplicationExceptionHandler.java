package test.task.conferencemanagement.exception.handling;

import test.task.conferencemanagement.exception.ConferenceIsUnavailableException;
import test.task.conferencemanagement.exception.EntityNofFoundException;
import test.task.conferencemanagement.exception.UserAlreadyExists;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EntityNofFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, createErrorResponseObject(ex), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(UserAlreadyExists.class)
    protected ResponseEntity<Object> handleUserAlreadyExists(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, createErrorResponseObject(ex), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(ConferenceIsUnavailableException.class)
    protected ResponseEntity<Object> handleConferenceAlreadyExists(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, createErrorResponseObject(ex), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    private Map<String, String> createErrorResponseObject(RuntimeException ex) {
        String errorMessage = ex.getMessage();
        Map<String, String> error = new HashMap<>();
        error.put("errorMessage", errorMessage);
        return error;
    }

}
