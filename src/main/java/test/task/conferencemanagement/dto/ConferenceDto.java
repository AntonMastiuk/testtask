package test.task.conferencemanagement.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Objects;
import java.util.Set;

public class ConferenceDto {
    private Long id;
    private String name;
    private int maxAvailableSeats;
    private boolean cancelled;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Set<UserDto> participants;

    public ConferenceDto() {
    }

    public ConferenceDto(ConferenceDto other) {
        this.id = other.id;
        this.name = other.name;
        this.maxAvailableSeats = other.maxAvailableSeats;
        this.cancelled = other.cancelled;
        this.participants = other.participants;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxAvailableSeats() {
        return maxAvailableSeats;
    }

    public void setMaxAvailableSeats(int maxAvailableSeats) {
        this.maxAvailableSeats = maxAvailableSeats;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Set<UserDto> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<UserDto> participants) {
        this.participants = participants;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConferenceDto that = (ConferenceDto) o;
        return maxAvailableSeats == that.maxAvailableSeats &&
            cancelled == that.cancelled &&
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, maxAvailableSeats, cancelled);
    }
}
