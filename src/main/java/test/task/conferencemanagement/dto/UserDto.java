package test.task.conferencemanagement.dto;

import java.util.Objects;

public class UserDto {
    private Long id;
    private String username;
    private String fullName;

    public UserDto() {
    }

    public UserDto(UserDto other) {
        this.id = other.id;
        this.username = other.username;
        this.fullName = other.fullName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(id, userDto.id) &&
            Objects.equals(username, userDto.username) &&
            Objects.equals(fullName, userDto.fullName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, fullName);
    }
}
