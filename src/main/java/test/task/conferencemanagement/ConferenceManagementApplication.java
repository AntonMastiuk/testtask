package test.task.conferencemanagement;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static test.task.conferencemanagement.swagger.SwaggerConstants.CONFERENCE_CONTROLLER_TAG;
import static test.task.conferencemanagement.swagger.SwaggerConstants.USER_CONTROLLER_TAG;

@SpringBootApplication
@OpenAPIDefinition(
    info = @Info(title = "Conference Management API", version = "1.0"),
    tags = {
        @Tag(name = USER_CONTROLLER_TAG, description = "User management"),
        @Tag(name = CONFERENCE_CONTROLLER_TAG, description = "Conference management")
    }
)
public class ConferenceManagementApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConferenceManagementApplication.class, args);
    }
}
