package test.task.conferencemanagement.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import test.task.conferencemanagement.dto.UserDto;
import test.task.conferencemanagement.service.UserService;

import java.util.List;

import static test.task.conferencemanagement.swagger.SwaggerConstants.*;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    @Operation(
        tags = USER_CONTROLLER_TAG,
        summary = CREATE_USER_SUMMARY,
        requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(content = {
            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserDto.class), examples = {
                @ExampleObject(value = CREATE_USER_REQUEST_BODY_EXAMPLE)
            })
        }),
        responses = {
            @ApiResponse(
                responseCode = STATUS_CREATED, description = CREATE_USER_CREATED_DESCRIPTION,
                content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserDto.class), examples = {
                        @ExampleObject(value = USER_RESPONSE_BODY_EXAMPLE)
                    })
                }),
            @ApiResponse(responseCode = STATUS_CONFLICT, description = USER_ALREADY_EXISTS_DESCRIPTION,
                content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                    @ExampleObject(value = ERROR_MESSAGE_USER_ALREADY_EXISTS_EXAMPLE)
                }))
        }
    )
    public UserDto createUser(@RequestBody UserDto userDto) {
        return userService.saveUser(userDto);
    }

    @PutMapping
    @Operation(
        tags = USER_CONTROLLER_TAG,
        summary = UPDATE_USER_SUMMARY,
        requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(content = {
            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserDto.class), examples = {
                @ExampleObject(value = UPDATE_USER_REQUEST_RESPONSE_BODY_EXAMPLE)
            })
        }),
        responses = {
            @ApiResponse(
                responseCode = STATUS_OK, description = UPDATE_USER_OK_DESCRIPTION,
                content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserDto.class), examples = {
                        @ExampleObject(value = UPDATE_USER_REQUEST_RESPONSE_BODY_EXAMPLE)
                    })
                }),
            @ApiResponse(responseCode = STATUS_CONFLICT, description = USER_ALREADY_EXISTS_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                @ExampleObject(value = ERROR_MESSAGE_USER_ALREADY_EXISTS_EXAMPLE)
            }))
        }
    )
    public UserDto updateUser(@RequestBody UserDto userDto) {
        return userService.saveUser(userDto);
    }

    @GetMapping
    @Operation(
        tags = USER_CONTROLLER_TAG,
        summary = GET_USERS_SUMMARY,
        responses = {
            @ApiResponse(
                responseCode = STATUS_OK,
                content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserDto.class), examples = {
                        @ExampleObject(value = MULTIPLE_USER_RESPONSE_BODY_EXAMPLE)
                    })})
        }
    )
    public List<UserDto> getUsers() {
        return userService.getAll();
    }

    @GetMapping("/{userId}")
    @Operation(
        tags = USER_CONTROLLER_TAG,
        summary = GET_USER_BY_ID_SUMMARY,
        parameters = {@Parameter(name = "userId", description = USER_ID_PARAM_DESCRIPTION)},
        responses = {
            @ApiResponse(
                responseCode = STATUS_OK,
                content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserDto.class), examples = {
                        @ExampleObject(USER_RESPONSE_BODY_EXAMPLE)
                    })
                }),
            @ApiResponse(responseCode = STATUS_NOT_FOUND, description = USER_WAS_NOT_FOUND_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                @ExampleObject(value = ERROR_MESSAGE_USER_NOT_FOUND_EXAMPLE)
            }))
        }
    )
    public UserDto getUser(@PathVariable("userId") Long userId) {
        return userService.findUserById(userId);
    }

}
