package test.task.conferencemanagement.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import test.task.conferencemanagement.dto.ConferenceDto;
import test.task.conferencemanagement.service.ConferenceService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static test.task.conferencemanagement.swagger.SwaggerConstants.*;

@RestController
@RequestMapping("/api/conferences")
public class ConferenceController {

    private final ConferenceService conferenceService;

    public ConferenceController(ConferenceService conferenceService) {
        this.conferenceService = conferenceService;
    }

    @GetMapping
    @Operation(
        tags = CONFERENCE_CONTROLLER_TAG,
        summary = GET_CONFERENCES_SUMMARY,
        responses = {
            @ApiResponse(
                responseCode = STATUS_OK,
                content = {
                    @Content(
                        mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ConferenceDto.class), examples = {
                        @ExampleObject(MULTIPLE_CONFERENCE_RESPONSE_BODY_WITHOUT_USERS_EXAMPLE)
                    })
                })
        }
    )
    public List<ConferenceDto> getConferences() {
        return this.conferenceService.getAll();
    }

    @GetMapping("/{conferenceId}")
    @Operation(
        tags = CONFERENCE_CONTROLLER_TAG,
        summary = GET_CONFERENCE_BY_ID_SUMMARY,
        parameters = {@Parameter(name = "conferenceId", description = CONFERENCE_ID_PARAM_DESCRIPTION)},
        responses = {
            @ApiResponse(
                responseCode = STATUS_OK,
                content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ConferenceDto.class), examples = {
                        @ExampleObject(CONFERENCE_RESPONSE_BODY_WITHOUT_USERS_EXAMPLE)
                    })
                }),
            @ApiResponse(responseCode = STATUS_NOT_FOUND, description = CONFERENCE_WAS_NOT_FOUND_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                @ExampleObject(value = ERROR_MESSAGE_CONFERENCE_NOT_FOUND_EXAMPLE)
            }))
        }
    )
    public ConferenceDto getConference(@PathVariable("conferenceId") Long conferenceId) {
        return conferenceService.getById(conferenceId);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    @Operation(
        tags = CONFERENCE_CONTROLLER_TAG,
        summary = CREATE_CONFERENCE_SUMMARY,
        requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(content = {
            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ConferenceDto.class), examples = {
                @ExampleObject(CREATE_CONFERENCE_REQUEST_BODY_EXAMPLE)
            })
        }),
        responses = {
            @ApiResponse(
                responseCode = STATUS_CREATED, description = CONFERENCE_WAS_CREATED_DESCRIPTION,
                content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ConferenceDto.class), examples = {
                        @ExampleObject(CONFERENCE_RESPONSE_BODY_WITHOUT_USERS_EXAMPLE)
                    })
                })
        }
    )
    public ConferenceDto createConference(@RequestBody ConferenceDto conferenceDto) {
        return this.conferenceService.saveConference(conferenceDto);
    }

    @PutMapping
    @Operation(
        tags = CONFERENCE_CONTROLLER_TAG,
        summary = UPDATE_CONFERENCE_SUMMARY,
        requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(content = {
            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ConferenceDto.class), examples = {
                @ExampleObject(UPDATE_CONFERENCE_REQUEST_RESPONSE_BODY)
            })
        }),
        responses = {
            @ApiResponse(
                responseCode = STATUS_OK, description = CONFERENCE_WAS_UPDATED_DESCRIPTION,
                content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ConferenceDto.class), examples = {
                        @ExampleObject(UPDATE_CONFERENCE_REQUEST_RESPONSE_BODY)
                    })
                })
        }
    )
    public ConferenceDto updateConference(@RequestBody ConferenceDto conferenceDto) {
        return this.conferenceService.saveConference(conferenceDto);
    }

    @PutMapping("/{conferenceId}/cancel")
    @Operation(
        tags = CONFERENCE_CONTROLLER_TAG,
        summary = CANCEL_CONFERENCE_SUMMARY,
        parameters = {@Parameter(name = "conferenceId", description = CONFERENCE_ID_PARAM_DESCRIPTION)},
        responses = {
            @ApiResponse(responseCode = STATUS_OK, description = CONFERENCE_WAS_CANCELLED_DESCRIPTION),
            @ApiResponse(responseCode = STATUS_NOT_FOUND, description = CONFERENCE_WAS_NOT_FOUND_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                @ExampleObject(value = ERROR_MESSAGE_CONFERENCE_NOT_FOUND_EXAMPLE)
            }))
        }
    )
    public void cancelConference(@PathVariable("conferenceId") Long conferenceId) {
        this.conferenceService.cancelConference(conferenceId);
    }

    @GetMapping("/{conferenceId}/availability")
    @Operation(
        tags = CONFERENCE_CONTROLLER_TAG,
        summary = IS_AVAILABLE_FOR_REGISTRATION_SUMMARY,
        description = IS_AVAILABLE_FOR_REGISTRATION_DESCRIPTION,
        parameters = {@Parameter(name = "conferenceId", description = CONFERENCE_ID_PARAM_DESCRIPTION)},
        responses = {
            @ApiResponse(
                responseCode = STATUS_OK, description = CONFERENCE_AVAILABILITY_RESULT_DESCRIPTION,
                content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                        @ExampleObject(name = CONFERENCE_AVAILABLE_NAME, value = CONFERENCE_AVAILABLE_EXAMPLE),
                        @ExampleObject(name = CONFERENCE_UNAVAILABLE_NAME, value = CONFERENCE_UNAVAILABLE_EXAMPLE),
                    })
                }),
            @ApiResponse(responseCode = STATUS_NOT_FOUND, description = CONFERENCE_WAS_NOT_FOUND_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                @ExampleObject(value = ERROR_MESSAGE_CONFERENCE_NOT_FOUND_EXAMPLE)
            }))
        }
    )
    public Map<String, Boolean> isAvailableForRegistration(@PathVariable("conferenceId") Long conferenceId) {
        Map<String, Boolean> result = new HashMap<>();
        boolean availableForRegistration = this.conferenceService.isAvailableForRegistration(conferenceId);
        result.put("isAvailable", availableForRegistration);
        return result;
    }

    @PostMapping("/{conferenceId}/register/{userId}")
    @Operation(
        tags = CONFERENCE_CONTROLLER_TAG,
        summary = REGISTER_FOR_CONFERENCE_SUMMARY,
        parameters = {@Parameter(name = "conferenceId", description = CONFERENCE_ID_PARAM_DESCRIPTION), @Parameter(name = "userId", description = USER_ID_PARAM_DESCRIPTION)},
        responses = {
            @ApiResponse(responseCode = STATUS_OK),
            @ApiResponse(responseCode = STATUS_NOT_FOUND, description = RESOURCE_WAS_NOT_FOUND_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                @ExampleObject(name = USER_WAS_NOT_FOUND_DESCRIPTION, value = ERROR_MESSAGE_USER_NOT_FOUND_DURING_CONFERENCE_REGISTRATION_EXAMPLE),
                @ExampleObject(name = CONFERENCE_WAS_NOT_FOUND_DESCRIPTION, value = ERROR_MESSAGE_CONFERENCE_NOT_FOUND_EXAMPLE)
            })),
            @ApiResponse(responseCode = STATUS_CONFLICT, description = CONFERENCE_REGISTRATION_FAILURE_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                @ExampleObject(name = CONFERENCE_REGISTRATION_FAILURE_CANCELLED_NAME, value = CONFERENCE_REGISTRATION_FAILURE_CANCELLED_EXAMPLE),
                @ExampleObject(name = CONFERENCE_REGISTRATION_FAILURE_NO_SEATS_NAME, value = CONFERENCE_REGISTRATION_FAILURE_NO_SEATS_EXAMPLE)
            }))
        }
    )
    public void registerForConference(@PathVariable("conferenceId") Long conferenceId, @PathVariable("userId") Long userId) {
        this.conferenceService.register(conferenceId, userId);
    }

    @PostMapping("/{id}/unregister/{userId}")
    @Operation(
        tags = CONFERENCE_CONTROLLER_TAG,
        summary = UNREGISTER_FOR_CONFERENCE_SUMMARY,
        responses = {
            @ApiResponse(
                responseCode = STATUS_OK,
                content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                        @ExampleObject(value = UNREGISTER_FROM_CONFERENCE_USER_WAS_MEMBER_EXAMPLE),
                        @ExampleObject(value = UNREGISTER_FROM_CONFERENCE_USER_WAS_NOT_MEMBER_EXAMPLE)
                    })
                }),
            @ApiResponse(responseCode = STATUS_NOT_FOUND, description = RESOURCE_WAS_NOT_FOUND_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                @ExampleObject(name = CONFERENCE_WAS_NOT_FOUND_DESCRIPTION, value = ERROR_MESSAGE_CONFERENCE_NOT_FOUND_EXAMPLE),
                @ExampleObject(name = USER_WAS_NOT_FOUND_DESCRIPTION, value = ERROR_MESSAGE_USER_NOT_FOUND_EXAMPLE)
            }))
        }
    )
    public Map<String, String> unregisterFromConference(@PathVariable("id") Long conferenceId, @PathVariable("userId") Long userId) {
        boolean unregister = this.conferenceService.unregister(conferenceId, userId);
        Map<String, String> unregisterResult = new HashMap<>();
        String message = unregister ? "User has been successfully unregistered" : "User is already not participant of the conference";
        unregisterResult.put("message", message);
        return unregisterResult;
    }

    @GetMapping("/{conferenceId}/participants")
    @Operation(
        tags = CONFERENCE_CONTROLLER_TAG,
        summary = GET_CONFERENCE_PARTICIPANTS_SUMMARY,
        parameters = @Parameter(name = "conferenceId", description = CONFERENCE_ID_PARAM_DESCRIPTION),
        responses = {
            @ApiResponse(
                responseCode = STATUS_OK,
                content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = ConferenceDto.class), examples = {
                        @ExampleObject(CONFERENCE_RESPONSE_BODY_WITH_USERS_EXAMPLE)
                    })
                }),
            @ApiResponse(responseCode = STATUS_NOT_FOUND, description = CONFERENCE_WAS_NOT_FOUND_DESCRIPTION, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                @ExampleObject(value = ERROR_MESSAGE_CONFERENCE_NOT_FOUND_EXAMPLE)
            }))
        }
    )
    public ConferenceDto getConferenceParticipants(@PathVariable("conferenceId") Long conferenceId) {
        return this.conferenceService.getParticipants(conferenceId);
    }

}
