CREATE SEQUENCE if not exists conference_seq
    MINVALUE 1
    MAXVALUE 999999999
    INCREMENT BY 1
    START WITH 3
    NOCACHE
    NOCYCLE;

CREATE SEQUENCE if not exists users_seq
    MINVALUE 1
    MAXVALUE 999999999
    INCREMENT BY 1
    START WITH 9
    NOCACHE
    NOCYCLE;

create table if not exists conference (
	id int8 primary key not null,
	name varchar (100) not null,
	maxavailableseats int not null,
	cancelled boolean not null default false
);

create table if not exists users (
	id int8 primary key not null,
	username varchar(25) not null unique,
	fullname varchar(50) not null
);

create table if not exists conference_users (
	conference_id int8 not null,
	users_id int8 not null,
	primary key (conference_id, users_id),
	foreign key (conference_id) references conference (id),
	foreign key (users_id) references users (id)
);

insert into conference(id, name, maxavailableseats) values(1, 'test', 6);
insert into conference(id, name, maxavailableseats) values(2, 'test-2', 3);
insert into users values(1, 'user1', 'first last');
insert into users values(2, 'user2', 'first last');
insert into users values(3, 'user3', 'first last');
insert into users values(4, 'user4', 'first last');
insert into users values(5, 'user5', 'first last');
insert into users values(6, 'user6', 'first last');
insert into users values(7, 'user7', 'first last');
insert into users values(8, 'user8', 'first last');
