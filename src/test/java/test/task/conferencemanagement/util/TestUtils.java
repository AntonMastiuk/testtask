package test.task.conferencemanagement.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class TestUtils {
    private TestUtils() {
        throw new UnsupportedOperationException();
    }

    public static String writeObjectToJson(Object o) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(o);
    }
}
