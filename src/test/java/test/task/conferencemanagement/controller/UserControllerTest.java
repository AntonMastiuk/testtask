package test.task.conferencemanagement.controller;

import test.task.conferencemanagement.dto.UserDto;
import test.task.conferencemanagement.exception.UserAlreadyExists;
import test.task.conferencemanagement.exception.UserNotFoundException;
import test.task.conferencemanagement.exception.handling.ApplicationExceptionHandler;
import test.task.conferencemanagement.service.UserService;
import test.task.conferencemanagement.util.TestUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class UserControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;
    @InjectMocks
    private ApplicationExceptionHandler applicationExceptionHandler;

    @BeforeEach
    public void init() {
        RestAssuredMockMvc.standaloneSetup(userController, applicationExceptionHandler);
    }

    @Test
    void createUser_ValidData_UserCreated() throws JsonProcessingException {
        UserDto userDto = new UserDto();
        userDto.setFullName("Test User");
        userDto.setUsername("testuser");

        UserDto createdUserDto = new UserDto(userDto);
        createdUserDto.setId(1L);

        when(userService.saveUser(userDto)).thenReturn(createdUserDto);

        given()
            .body(userDto)
            .contentType(ContentType.JSON)
            .post("/api/users")
            .then()
            .statusCode(HttpStatus.CREATED.value())
            .body(Matchers.is(equalTo(TestUtils.writeObjectToJson(createdUserDto))));

        verify(userService, times(1)).saveUser(any());
    }

    @Test
    void createUser_UsernameExists_ExceptionThrown() {
        UserDto userDto = new UserDto();
        userDto.setFullName("Test User");
        String username = "testuser";
        userDto.setUsername(username);

        String errorMessage = String.format("User %s already exists", username);
        when(userService.saveUser(userDto)).thenThrow(new UserAlreadyExists(errorMessage));

        given()
            .body(userDto)
            .contentType(ContentType.JSON)
            .post("/api/users")
            .then()
            .statusCode(HttpStatus.CONFLICT.value())
            .body(containsString(errorMessage));

        verify(userService, times(1)).saveUser(any());
    }

    @Test
    void updateUser_ValidData_UserUpdated() throws JsonProcessingException {
        UserDto userDto = new UserDto();
        userDto.setId(1L);
        userDto.setFullName("Test User");
        userDto.setUsername("testuser");

        when(userService.saveUser(userDto)).thenReturn(userDto);

        given()
            .body(userDto)
            .contentType(ContentType.JSON)
            .put("/api/users")
            .then()
            .statusCode(HttpStatus.OK.value())
            .body(is(equalTo(TestUtils.writeObjectToJson(userDto))));

        verify(userService, times(1)).saveUser(any());
    }

    @Test
    void updateUser_UsernameExists_ExceptionThrown() {
        UserDto userDto = new UserDto();
        userDto.setId(1L);
        userDto.setFullName("Test User");
        String username = "testuser";
        userDto.setUsername(username);

        String errorMessage = String.format("User %s already exists", username);
        when(userService.saveUser(userDto)).thenThrow(new UserAlreadyExists(errorMessage));

        given()
            .body(userDto)
            .contentType(ContentType.JSON)
            .put("/api/users")
            .then()
            .statusCode(HttpStatus.CONFLICT.value())
            .body(containsString(errorMessage));

        verify(userService, times(1)).saveUser(any());
    }


    @Test
    void getUsers_UsersArePresent_AllReturned() throws JsonProcessingException {
        List<UserDto> userDtos = new ArrayList<>();
        for (long i = 1; i < 10; i++) {
            UserDto userDto = new UserDto();
            userDto.setId(i);
            userDto.setFullName("Test User");
            userDto.setUsername("testuser" + i);
            userDtos.add(userDto);
        }

        when(userService.getAll()).thenReturn(userDtos);

        given()
            .when()
            .get("/api/users")
            .then()
            .statusCode(HttpStatus.OK.value())
            .contentType(ContentType.JSON)
            .body(is(equalTo(TestUtils.writeObjectToJson((userDtos)))));

        verify(userService, times(1)).getAll();
    }

    @Test
    void getUsers_UsersAreAbsent_EmptyArrayReturned() {
        when(userService.getAll()).thenReturn(Collections.emptyList());

        given()
            .when()
            .get("/api/users")
            .then()
            .statusCode(HttpStatus.OK.value())
            .contentType(ContentType.JSON)
            .body(is(equalTo("[]")));

        verify(userService, times(1)).getAll();
    }

    @Test
    void getUser_UserExists_UserReturned() throws JsonProcessingException {
        UserDto userDto = new UserDto();
        long userId = 1L;
        userDto.setId(userId);
        userDto.setFullName("Test User");
        userDto.setUsername("testuser");

        when(userService.findUserById(userId)).thenReturn(userDto);

        given()
            .when()
            .get("/api/users/" + userId)
            .then()
            .statusCode(HttpStatus.OK.value())
            .body(is(equalTo(TestUtils.writeObjectToJson(userDto))));

        verify(userService, times(1)).findUserById(any());
    }

    @Test
    void getUser_UserDoesntExists_404() {
        Long userId = 1L;
        UserDto userDto = new UserDto();
        userDto.setId(userId);
        userDto.setFullName("Test User");
        userDto.setUsername("testuser");

        String errorMessage = String.format("User with id %d was not found", userId);
        when(userService.findUserById(userId)).thenThrow(new UserNotFoundException(errorMessage));

        given()
            .when()
            .get("/api/users/" + userId)
            .then()
            .statusCode(HttpStatus.NOT_FOUND.value())
            .body(containsString(errorMessage));

        verify(userService, times(1)).findUserById(any());
    }
}
