package test.task.conferencemanagement.controller;

import test.task.conferencemanagement.dto.ConferenceDto;
import test.task.conferencemanagement.dto.UserDto;
import test.task.conferencemanagement.exception.ConferenceIsUnavailableException;
import test.task.conferencemanagement.exception.ConferenceNotFoundException;
import test.task.conferencemanagement.exception.UserNotFoundException;
import test.task.conferencemanagement.exception.handling.ApplicationExceptionHandler;
import test.task.conferencemanagement.service.ConferenceService;
import test.task.conferencemanagement.util.TestUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.*;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class ConferenceControllerTest {

    @Mock
    private ConferenceService conferenceService;
    @InjectMocks
    private ConferenceController conferenceController;
    @InjectMocks
    private ApplicationExceptionHandler applicationExceptionHandler;

    @BeforeEach
    public void init() {
        RestAssuredMockMvc.standaloneSetup(conferenceController, applicationExceptionHandler);
    }

    @Test
    void getConferences_ConferencesArePresent_AllReturned() throws JsonProcessingException {
        List<ConferenceDto> conferenceDtoList = new ArrayList<>();
        for (long i = 1; i < 10; i++) {
            ConferenceDto conference = new ConferenceDto();
            conference.setId(i);
            conference.setMaxAvailableSeats((int) (10 * i));
            conference.setName("Awesome conference #" + i);
            conferenceDtoList.add(conference);
        }

        when(conferenceService.getAll()).thenReturn(conferenceDtoList);

        given()
            .when()
            .get("/api/conferences")
            .then().statusCode(HttpStatus.OK.value())
            .contentType(ContentType.JSON)
            .body(is(equalTo(TestUtils.writeObjectToJson(conferenceDtoList))));

        verify(conferenceService, times(1)).getAll();
    }

    @Test
    void getConferences_ConferencesAreAbsent_EmptyArrayReturned() {
        when(conferenceService.getAll()).thenReturn(Collections.emptyList());

        given()
            .when()
            .get("/api/conferences")
            .then().statusCode(HttpStatus.OK.value())
            .contentType(ContentType.JSON)
            .body(is(equalTo("[]")));

        verify(conferenceService, times(1)).getAll();
    }

    @Test
    void getConference_ConferenceExists_ConferenceReturned() throws JsonProcessingException {
        ConferenceDto conference = new ConferenceDto();
        conference.setId(1L);
        conference.setMaxAvailableSeats(10);
        conference.setName("Awesome conference #1");

        when(conferenceService.getById(1L)).thenReturn(conference);

        given()
            .when()
            .get("/api/conferences/" + 1)
            .then().statusCode(HttpStatus.OK.value())
            .contentType(ContentType.JSON)
            .body(is(equalTo(TestUtils.writeObjectToJson(conference))));

        verify(conferenceService, times(1)).getById(anyLong());
    }

    @Test
    void getConference_ConferenceDoesntExists_404() {
        String errorMessage = String.format("Conference with id %d was not found", 1L);
        when(conferenceService.getById(1L)).thenThrow(new ConferenceNotFoundException(errorMessage));

        given()
            .when()
            .get("/api/conferences/" + 1)
            .then().statusCode(HttpStatus.NOT_FOUND.value())
            .contentType(ContentType.JSON)
            .body(containsString(errorMessage));

        verify(conferenceService, times(1)).getById(anyLong());
    }

    @Test
    void createConference_ValidData_ConferenceCreated() throws JsonProcessingException {
        ConferenceDto conference = new ConferenceDto();
        conference.setMaxAvailableSeats(10);
        conference.setName("Awesome conference #1");

        ConferenceDto createdConference = new ConferenceDto(conference);
        createdConference.setId(1L);

        when(conferenceService.saveConference(conference)).thenReturn(createdConference);
        given()
            .body(conference)
            .contentType(ContentType.JSON)
            .post("/api/conferences")
            .then().statusCode(HttpStatus.CREATED.value())
            .body(is(equalTo(TestUtils.writeObjectToJson(createdConference))));

        verify(conferenceService, times(1)).saveConference(any());
    }

    @Test
    void updateConference_ValidData_ConferenceCreated() throws JsonProcessingException {
        ConferenceDto conference = new ConferenceDto();
        conference.setId(1L);
        conference.setMaxAvailableSeats(10);
        conference.setName("Awesome conference #1");

        when(conferenceService.saveConference(conference)).thenReturn(conference);

        given()
            .body(conference)
            .contentType(ContentType.JSON)
            .when()
            .put("/api/conferences")
            .then().statusCode(HttpStatus.OK.value())
            .body(is(equalTo(TestUtils.writeObjectToJson(conference))));

        verify(conferenceService, times(1)).saveConference(any());
    }

    @Test
    void cancelConference_ValidConferenceIdProvided_ConferenceCanceled() {
        Long conferenceId = 1L;
        doNothing().when(conferenceService).cancelConference(conferenceId);

        given()
            .when()
            .put("/api/conferences/" + conferenceId + "/cancel")
            .then().statusCode(HttpStatus.OK.value());

        verify(conferenceService, times(1)).cancelConference(anyLong());
    }

    @Test
    void cancelConference_InvalidConferenceIdProvided_404() {
        Long conferenceId = 1L;
        String errorMessage = String.format("Conference with id %d was not found", conferenceId);
        doThrow(new ConferenceNotFoundException(errorMessage)).when(conferenceService).cancelConference(conferenceId);

        given()
            .when()
            .put("/api/conferences/" + conferenceId + "/cancel")
            .then().statusCode(HttpStatus.NOT_FOUND.value())
            .contentType(ContentType.JSON)
            .body(containsString(errorMessage));

        verify(conferenceService, times(1)).cancelConference(anyLong());
    }

    @Test
    void isAvailableForRegistration_ValidConferenceIdProvided_True() throws JsonProcessingException {
        Long conferenceId = 1L;
        boolean isAvailable = true;
        when(conferenceService.isAvailableForRegistration(conferenceId)).thenReturn(isAvailable);

        given()
            .when()
            .get("/api/conferences/" + conferenceId + "/availability")
            .then().statusCode(HttpStatus.OK.value())
            .contentType(ContentType.JSON)
            .body(containsString(String.valueOf(isAvailable)));

        verify(conferenceService, times(1)).isAvailableForRegistration(anyLong());
    }

    @Test
    void isAvailableForRegistration_ValidConferenceIdProvided_False() throws JsonProcessingException {
        Long conferenceId = 1L;
        boolean isAvailable = false;
        when(conferenceService.isAvailableForRegistration(conferenceId)).thenReturn(isAvailable);

        given()
            .when()
            .get("/api/conferences/" + conferenceId + "/availability")
            .then().statusCode(HttpStatus.OK.value())
            .contentType(ContentType.JSON)
            .body(containsString(String.valueOf(isAvailable)));

        verify(conferenceService, times(1)).isAvailableForRegistration(anyLong());
    }

    @Test
    void isAvailableForRegistration_invalidConferenceIdProvided_404() {
        Long conferenceId = 1L;
        String errorMessage = String.format("Conference with id %d was not found", conferenceId);
        when(conferenceService.isAvailableForRegistration(conferenceId)).thenThrow(new ConferenceNotFoundException(errorMessage));

        given()
            .when()
            .get("/api/conferences/" + conferenceId + "/availability")
            .then().statusCode(HttpStatus.NOT_FOUND.value())
            .contentType(ContentType.JSON)
            .body(containsString(errorMessage));

        verify(conferenceService, times(1)).isAvailableForRegistration(anyLong());
    }

    @Test
    void registerForConference_ConferenceAndUserExistAndConferenceIsAvailable_200() {
        Long conferenceId = 1L;
        Long userId = 1L;
        doNothing().when(conferenceService).register(conferenceId, userId);

        given()
            .when()
            .post("/api/conferences/" + conferenceId + "/register/" + userId)
            .then().statusCode(HttpStatus.OK.value());

        verify(conferenceService, times(1)).register(anyLong(), anyLong());
    }

    @Test
    void registerForConference_ConferenceDoesntExists_404() {
        Long conferenceId = 1L;
        Long userId = 1L;
        String errorMessage = String.format("Conference with id %d was not found", conferenceId);
        doThrow(new ConferenceNotFoundException(errorMessage)).when(conferenceService).register(conferenceId, userId);

        given()
            .when()
            .post("/api/conferences/" + conferenceId + "/register/" + userId)
            .then().statusCode(HttpStatus.NOT_FOUND.value())
            .contentType(ContentType.JSON)
            .body(containsString(errorMessage));

        verify(conferenceService, times(1)).register(anyLong(), anyLong());
    }

    @Test
    void registerForConference_UserDoesntExists_404() {
        Long conferenceId = 1L;
        Long userId = 1L;
        String errorMessage = String.format("User with id %d could not register for conference, because it doesn't exists", userId);
        doThrow(new UserNotFoundException(errorMessage)).when(conferenceService).register(conferenceId, userId);

        given()
            .when()
            .post("/api/conferences/" + conferenceId + "/register/" + userId)
            .then().statusCode(HttpStatus.NOT_FOUND.value())
            .contentType(ContentType.JSON)
            .body(containsString(errorMessage));

        verify(conferenceService, times(1)).register(anyLong(), anyLong());
    }

    @Test
    void registerForConference_ConferenceAndUserExistAndConferenceIsCancelled_409() {
        Long conferenceId = 1L;
        Long userId = 1L;
        String errorMessage = "The conference has been cancelled";
        doThrow(new ConferenceIsUnavailableException(errorMessage)).when(conferenceService).register(conferenceId, userId);

        given()
            .when()
            .post("/api/conferences/" + conferenceId + "/register/" + userId)
            .then().statusCode(HttpStatus.CONFLICT.value())
            .contentType(ContentType.JSON)
            .body(containsString(errorMessage));

        verify(conferenceService, times(1)).register(anyLong(), anyLong());
    }

    @Test
    void registerForConference_ConferenceAndUserExistAndThereAreNoAvailableSeats_409() {
        Long conferenceId = 1L;
        Long userId = 1L;
        String errorMessage = "There are no available seats";
        doThrow(new ConferenceIsUnavailableException(errorMessage)).when(conferenceService).register(conferenceId, userId);

        given()
            .when()
            .post("/api/conferences/" + conferenceId + "/register/" + userId)
            .then().statusCode(HttpStatus.CONFLICT.value())
            .contentType(ContentType.JSON)
            .body(containsString(errorMessage));

        verify(conferenceService, times(1)).register(anyLong(), anyLong());
    }

    @Test
    void unregisterFromConference_ConferenceExistsAndUserRegistered_successfulConfirmationReceived() throws JsonProcessingException {
        Long conferenceId = 1L;
        Long userId = 1L;
        Map<String, String> unregisterResult = new HashMap<>();
        unregisterResult.put("message", "User has been successfully unregistered");
        when(conferenceService.unregister(conferenceId, userId)).thenReturn(true);

        given()
            .when()
            .post("/api/conferences/" + conferenceId + "/unregister/" + userId)
            .then().statusCode(HttpStatus.OK.value())
            .body(is(equalTo(TestUtils.writeObjectToJson(unregisterResult))));

        verify(conferenceService, times(1)).unregister(anyLong(), anyLong());
    }

    @Test
    void unregisterFromConference_ConferenceExistsAndUserIsNotParticipant_unsuccessfulConfirmationReceived() throws JsonProcessingException {
        Long conferenceId = 1L;
        Long userId = 1L;
        Map<String, String> unregisterResult = new HashMap<>();
        unregisterResult.put("message", "User is already not participant of the conference");
        when(conferenceService.unregister(conferenceId, userId)).thenReturn(false);

        given()
            .when()
            .post("/api/conferences/" + conferenceId + "/unregister/" + userId)
            .then().statusCode(HttpStatus.OK.value())
            .body(is(equalTo(TestUtils.writeObjectToJson(unregisterResult))));

        verify(conferenceService, times(1)).unregister(anyLong(), anyLong());
    }

    @Test
    void unregisterFromConference_ConferenceDoesntExists_404() {
        Long conferenceId = 1L;
        Long userId = 1L;
        String errorMessage = String.format("Conference with id %d was not found", conferenceId);
        doThrow(new ConferenceNotFoundException(errorMessage)).when(conferenceService).unregister(conferenceId, userId);

        given()
            .when()
            .post("/api/conferences/" + conferenceId + "/unregister/" + userId)
            .then().statusCode(HttpStatus.NOT_FOUND.value())
            .contentType(ContentType.JSON)
            .body(containsString(errorMessage));

        verify(conferenceService, times(1)).unregister(anyLong(), anyLong());
    }

    @Test
    void unregisterFromConference_UserDoesntExists_404() {
        Long conferenceId = 1L;
        Long userId = 1L;
        String errorMessage = String.format("User with id %d was not found", userId);
        doThrow(new UserNotFoundException(errorMessage)).when(conferenceService).unregister(conferenceId, userId);

        given()
            .when()
            .post("/api/conferences/" + conferenceId + "/unregister/" + userId)
            .then().statusCode(HttpStatus.NOT_FOUND.value())
            .contentType(ContentType.JSON)
            .body(containsString(errorMessage));

        verify(conferenceService, times(1)).unregister(anyLong(), anyLong());
    }

    @Test
    void getConferenceParticipants_ConferenceExists_DataReturnedSuccessfully() throws JsonProcessingException {
        Long conferenceId = 1L;
        ConferenceDto conference = new ConferenceDto();
        conference.setId(1L);
        conference.setMaxAvailableSeats(10);
        conference.setName("Awesome conference #1");
        Set<UserDto> participants = new HashSet<>();
        UserDto userDto = new UserDto();
        userDto.setId(1L);
        userDto.setFullName("Full Name");
        userDto.setUsername("username");
        participants.add(userDto);
        conference.setParticipants(participants);
        when(conferenceService.getParticipants(conferenceId)).thenReturn(conference);

        given()
            .when()
            .get("/api/conferences/" + conferenceId + "/participants")
            .then().statusCode(HttpStatus.OK.value())
        .body(is(equalTo(TestUtils.writeObjectToJson(conference))));

        verify(conferenceService, times(1)).getParticipants(anyLong());
    }

}
